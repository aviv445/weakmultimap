import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

public class WeakLink<K, T> extends WeakReference<T> {
    K link;

    public WeakLink(K link, T referent, ReferenceQueue<? super T> q) {
        super(referent, q);
        this.link = link;
    }

    public K getLink(){
        return link;
    }
}
