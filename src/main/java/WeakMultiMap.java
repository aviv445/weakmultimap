
import java.lang.ref.ReferenceQueue;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;

/**
 * A key <K> to {@link ArrayList} of type <T> map.
 * Each element in the {@link Set} is Weak Referenced,
 * when a key's {@link ArrayList} is empty the entry is removed.
 * @param <K> - Key type
 * @param <V> - Value type
 */
public class WeakMultiMap<K, V> {

    // Reference queue to detect which objects are collected by GC
    private final ReferenceQueue<V> _garbageQueue;
    private final Lock _staleLock;

    private final Map<K, Collection<WeakLink<K, V>>> _linkHashMap;
    private final ReadWriteLock _linkMapModifyLock;

    private final Consumer<K> _onLinkRemove;

    public WeakMultiMap() {
        this(null);
    }

    public WeakMultiMap(Consumer<K> onLinkRemove) {
        _onLinkRemove = onLinkRemove;
        _linkHashMap = new ConcurrentHashMap<>();
        _garbageQueue = new ReferenceQueue<>();

        _linkMapModifyLock = new ReentrantReadWriteLock();
        _staleLock = new ReentrantLock();
    }

    /**
     * Remove links which has been GCed.
     */
    private void expungeStaleEntries() {
        // Try acquiring the lock,
        // if someone else already cleaning the stale links.
        if(_staleLock.tryLock()) {
            try {
                for (Object o; (o = _garbageQueue.poll()) != null; ) {
                    @SuppressWarnings("unchecked")
                    K link = ((WeakLink<K, V>)o).getLink();
                    Collection<WeakLink<K, V>> linkedObjects = _linkHashMap.get(link);
                    linkedObjects.remove(o);

                    if (linkedObjects.isEmpty()) {
                        // Don't allow adding links concurrently
                        _linkMapModifyLock.writeLock().lock();
                        try {
                            // Recheck the linked objects are still empty,
                            // a link could have been concurrently added.
                            if (linkedObjects.isEmpty()) {
                                _linkHashMap.remove(link);

                                // Notify the link has been removed
                                if(_onLinkRemove != null) {
                                    _onLinkRemove.accept(link);
                                }
                            }
                        } finally {
                            _linkMapModifyLock.writeLock().unlock();
                        }
                    }
                }
            } finally {
                _staleLock.unlock();
            }
        }
    }

    public void put(K key, V value){
        // Don't allow removing links concurrently
        _linkMapModifyLock.readLock().lock();
        try {
            // Create a set in case no set exists
            _linkHashMap.computeIfAbsent(key, (k) -> ConcurrentHashMap.newKeySet());
            Collection<WeakLink<K, V>> objects = _linkHashMap.get(key);
            objects.add(new WeakLink<>(key, value, _garbageQueue));
        } finally {
            _linkMapModifyLock.readLock().unlock();
        }

        // Cleanup links which are pointing to null
        expungeStaleEntries();
    }

    public Collection<V> get(K key){
        Collection<V> values;

        Collection<WeakLink<K, V>> links = _linkHashMap.get(key);
        if(links == null || links.isEmpty()) {
            values = new ArrayList<>(0);
        }else {
            // Make a strong reference to the values.
            // Insert into a set to ensure unique values.
            values = new HashSet<>(links.size());
            for (WeakLink<K, V> wl : links) {
                V value = wl.get();
                if (value != null) {
                    values.add(value);
                }
            }
        }

        return values;
    }
}