import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class WeakMultiMapTest {

    ExecutorService executor =
            new ThreadPoolExecutor(100, 100, 0L, TimeUnit.MILLISECONDS,
                    new ArrayBlockingQueue<>(100_000),
                    new ThreadPoolExecutor.CallerRunsPolicy());

    @Test
    public void concurrencyPutBenchMark() throws InterruptedException {
        WeakMultiMap<Integer, String> warmUpMap = new WeakMultiMap<>();

        // Warm up
        for (int i = 0; i < 500_000; i++) {
            String s = Integer.toString(i);
            final Integer key = i % 100;
            warmUpMap.put(key, s);
        }

        final long ITERATIONS = 1_000_000;
        AtomicLong sumDuration = new AtomicLong(0);
        WeakMultiMap<Integer, String> map = new WeakMultiMap<>();

        for(int i=0; i < ITERATIONS; i++) {
            String s = Integer.toString(i);
            final Integer key = i % 100;
            executor.execute(() -> {
                long start = System.nanoTime();
                map.put(key, s);
                long end = System.nanoTime();
                sumDuration.addAndGet(end - start);
            });
        }

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.HOURS);

        System.out.println(String.format("Sum time: %s nano seconds", sumDuration.get()));
        System.out.println(String.format("Average time: %s nano seconds", sumDuration.get() / ITERATIONS));
    }

    @Test
    public void concurrencyPut() throws InterruptedException {
        WeakMultiMap<Integer, String> map = new WeakMultiMap<>();

        final Integer key = 1;
        Collection<String> values = new ArrayList<>(1_000_000);
        for(int i=0; i < 1_000_000; i++) {
            String s = Integer.toString(i);
            values.add(s);
            executor.execute(() -> map.put(key, s));
        }

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.HOURS);

        Assert.assertEquals(map.get(key).size(), values.size());
    }

    @Test
    public void concurrencyPutGet() throws InterruptedException {
        WeakMultiMap<Integer, String> map = new WeakMultiMap<>();

        final Integer key = 1;
        for(int i=0; i < 1_000_000; i++) {
            final int fi = i;
            executor.execute(() -> map.put(key, Integer.toString(fi)));
        }

        executor.shutdown();

        while(!executor.isTerminated()) {
            try {
                for(int i=0; i < 1_000_000; i++) {
                    map.get(i);
                }
            }catch(Exception e) {
                Assert.fail(e.getMessage());
            }
        }

        executor.awaitTermination(1, TimeUnit.HOURS);
    }

    @Test
    public void concurrencyStaleLinksCleanup() throws InterruptedException {
        WeakMultiMap<Integer, String> map = new WeakMultiMap<>();

        final Integer key = 1;
        Collection<String> values = new ArrayList<>(1_000_000);
        for(int i=0; i < 1_000_000; i++) {
            final String a = Integer.toString(i);
            if(i%2 == 0) {
                values.add(a);
            }
            executor.execute(() -> map.put(key, a));
        }

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.DAYS);
        System.gc();
        Thread.sleep(1_000);
        map.put(2, "");
        Assert.assertEquals(values.size(), map.get(key).size());
    }


    @Test
    public void concurrencyStaleLinksCleanupRemoveKey() throws InterruptedException {
        WeakMultiMap<Integer, String> map = new WeakMultiMap<>();

        final Integer key = 1;
        for(int i=0; i < 1_000_000; i++) {
            final int fi = i % 10;
            executor.execute(() -> map.put(fi, Integer.toString(fi)));
        }

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.DAYS);
        System.gc();
        Thread.sleep(1_000);
        map.put(2, "");
        Assert.assertEquals(0, map.get(key).size());
    }
}
